This is a Spring Boot Application fetching weather based on city.

The application uses https://openweathermap.org/ to fetch data.
It needs a file 'api_details.properties' to get the url and api key.
The property file needs to have this content:
```
url=api.openweathermap.org/data/2.5/weather
api_key=<secret api key>
```
The application accepts following jvm arguments:
```
-Dspring.profiles.active=local (optional)
-Dweather-api-file-path=<location of api_details.properties file> (mandatory)
```
Application will fail to start if the property key file location is not passed or the file is missing.

Once the application is started it will be avaliable on http://localhost:8080/weather.
