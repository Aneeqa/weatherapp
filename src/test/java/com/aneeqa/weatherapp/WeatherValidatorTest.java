package com.aneeqa.weatherapp;

import com.aneeqa.weatherapp.validator.WeatherValidator;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class WeatherValidatorTest {

    @InjectMocks
    WeatherValidator weatherValidator;

    @Test
    public void testValidateCity_valid() {
        assertTrue(weatherValidator.validateCity("Sydney"));
        assertTrue(weatherValidator.validateCity("San Francisco"));
    }

    @Test
    public void testValidateCity_invalid() {
        assertFalse(weatherValidator.validateCity("1"));
        assertFalse(weatherValidator.validateCity("&%"));
    }

    @Test
    public void testValidateUnit_valid() {
        assertTrue(weatherValidator.validateUnit("metric"));
        assertTrue(weatherValidator.validateUnit("imperial"));
    }

    @Test
    public void testValidateUnit_invalid() {
        assertFalse(weatherValidator.validateUnit("abc"));
    }
}
