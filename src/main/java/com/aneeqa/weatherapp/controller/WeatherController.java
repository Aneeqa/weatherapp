package com.aneeqa.weatherapp.controller;

import com.aneeqa.weatherapp.config.WeatherApiDetails;
import com.aneeqa.weatherapp.model.ErrorModel;
import com.aneeqa.weatherapp.model.WeatherDetails;
import com.aneeqa.weatherapp.validator.WeatherValidator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Controller
public class WeatherController {

    public static final String error = "error";

    @Autowired
    private WeatherApiDetails weatherApiDetails;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    WeatherValidator weatherValidator;

    @GetMapping("/weather")
    public String weather() {
        return "weather";
    }

    @Cacheable("weather")
    @GetMapping("/weather/find")
    public String findWeather(@RequestParam String city, @RequestParam String unit, Model model) throws JsonProcessingException {

        weatherValidator.validateCityAndUnit(city, unit, model);
        if (model.containsAttribute(WeatherValidator.error)) {
            return "weather";
        }
        UriComponents uriComponents = UriComponentsBuilder
                .newInstance()
                .scheme("http")
                .host(weatherApiDetails.getUrl())
                .path("")
                .query("q={city}&units={unit}&APPID={key}")
                .buildAndExpand(city, unit, weatherApiDetails.getKey());

        String uri = uriComponents.toUriString();

        ResponseEntity<String> resp;
        ObjectMapper mapper = new ObjectMapper();
        try {
            resp = restTemplate.exchange(uri, HttpMethod.GET, null, String.class);
            WeatherDetails weatherDetails = mapper.readValue(resp.getBody(), WeatherDetails.class);
            model.addAttribute("weatherDetails", weatherDetails);
        } catch (HttpClientErrorException e) {
            String errorMessage = mapper.readValue(e.getResponseBodyAsString(), ErrorModel.class).getMessage();
            model.addAttribute(error, StringUtils.capitalize(errorMessage));
        } catch (Exception e) {
            model.addAttribute(error, e.getMessage());
        }
        return "weather";
    }

}
