package com.aneeqa.weatherapp.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Configuration
@PropertySource(value = "${weather-api-details.propeties.file.location}")
@Component
public class WeatherApiDetails {

    public static final String API_URL = "${url}";
    public static final String API_KEY = "${api_key}";

    @Value(API_URL)
    private String url;

    @Value(API_KEY)
    private String key;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
