package com.aneeqa.weatherapp.model;

public enum TempUnit {

    CELSIUS("metric"),
    FAHRENHEIT("imperial");

    public final String value;

    TempUnit(String value) {
        this.value = value;
    }

    public static TempUnit findByValue(String value){
        for(TempUnit v : values()){
            if( v.value.equals(value)){
                return v;
            }
        }
        return null;
    }
}
