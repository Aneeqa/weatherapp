package com.aneeqa.weatherapp.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherDetails implements Serializable {

    private List<Weather> weather;
    private Main main;

    @JsonProperty("weather")
    public List<Weather> getWeather() {
        return weather;
    }

    public void setWeather(List<Weather> weather) {
        this.weather = weather;
    }

    @JsonProperty("main")
    public Main getMain() {
        return main;
    }

    public void setMain(Main main) {
        this.main = main;
    }

}