package com.aneeqa.weatherapp.validator;

import com.aneeqa.weatherapp.model.TempUnit;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;

@Component
public class WeatherValidator {

    public static final String error = "error";

    public void validateCityAndUnit(String city, String unit, Model model) {
        if (StringUtils.isEmpty(city)) {
            model.addAttribute(error, "Please enter a city name");
            return;
        }

        boolean isCityInvalid = !validateCity(city);
        boolean isUnitInvalid = !validateUnit(unit);
        if (isCityInvalid && isUnitInvalid) {
            model.addAttribute(error, "Invalid city and unit");
        } else if (isCityInvalid) {
            model.addAttribute(error, "Invalid city");
        } else if(isUnitInvalid) {
            model.addAttribute(error, "Invalid unit");
        }
    }

    public boolean validateCity(String city) {
        return city.matches("^[a-zA-Z ]*$");
    }

    public boolean validateUnit(String unit) {
        return TempUnit.findByValue(unit) != null;
    }
}
