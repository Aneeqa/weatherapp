<%@ page errorPage="error.jsp" %>
<div>
    <br/>
    <div ${error == null ? '' : 'hidden'}>
        <div class="row">
            <div class="col-sm-4">
                <p class="weather-data text-large">Temperature:</p>
            </div>
            <div class="col-sm-8">
                <p class="text-large">${weatherDetails.getMain().getTemp()}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <p class="weather-data text-large">Description:</p>
            </div>
            <div class="col-sm-8">
                <p class="text-capitalize text-large"> ${weatherDetails.getWeather().get(0).getDescription()}</p>
            </div>

        </div>

    </div>

    <div ${error != null ? '' : 'hidden'}>
        <br/>
        <h4 class="text-danger">${error}</h4>
        <br/>
    </div>

</div>
