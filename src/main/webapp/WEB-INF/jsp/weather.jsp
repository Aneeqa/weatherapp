<%@ page errorPage="error.jsp" %>

<html lang="en">
<head>
    <title>My Weather App</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/css/style.css"/>
</head>
<body>

<div class="container">
    <form class="form-horizontal" action="/weather/find" method="GET">
        <div class="form-group myform">
            <div class="row">
                <div class="col-sm-2">
                    <p class="control-label text-large" for="city">City</p>
                </div>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="city" placeholder="Enter city" name="city">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col-sm-2">
                    <p class="control-label text-large" for="unit">Unit</p>
                </div>
                <div class="col-sm-4">
                    <select id="unit" name="unit" class="form-control">
                        <option value="metric" selected>Celsius</option>
                        <option value="imperial">Fahrenheit</option>
                    </select>
                </div>
            </div>

        </div>
        <br/>
        <br/>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary">Fetch</button>
            </div>
        </div>
    </form>


    <div class="col-sm-offset-2 col-sm-4" ${weatherDetails != null || error != null ? '' : 'hidden'}>
        <%@ include file="weatherDetails.jsp" %>
    </div>
</div>
</div>

</body>
</html>